const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

	//NAME
		if(!req.body.hasOwnProperty("name")){
			return res.status(400).send({
				error: "Bad Request : missing required parameter NAME"
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				error: "Bad Request : please enter a string value only"
			})
		}

		if(req.body.name === ""){
			return res.status(400).send({
				error: "Bad Request : please enter value in NAME"
			})
		}
	//EX
		if(!req.body.hasOwnProperty("ex")){
			return res.status(400).send({
				error: "Bad Request : missing required parameter EX"
			})
		}

		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				error: "Bad Request : please enter an object"
			})
		}

		if(req.body.ex === ""){
			return res.status(400).send({
				error: "Bad Request : please enter value in EX"
			})
		}
	//ALIAS
		if(!req.body.hasOwnProperty("alias")){
			return res.status(400).send({
				error: "Bad Request : missing required parameter ALIAS"
			})
		}

		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				error: "Bad Request : please enter a string value only"
			})
		}

		if(req.body.alias === ""){
			return res.status(400).send({
				error: "Bad Request : please enter value in ALIAS"
			})
		}

		let duplicateCheck = exchangeRates.find((alias) => {
			return alias.alias === req.body.alias});

		if(duplicateCheck){
			return res.status(400).send({
				error: "Bad Request : ALIAS is duplicated"
			})
		}

		return res.status(200).send();
	})

module.exports = router;

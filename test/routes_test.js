const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	
	it('test_api_post_currency_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/currency')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})


//NAME
	it('test_api_post_currency_returns_400_if_name_is_missing',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'dummy',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_name_is_not_string',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'dummy',
			name: 42,
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_name_is_empty',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'dummy',
			name: "",
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

//EX

	it('test_api_post_currency_returns_400_if_EX_is_missing',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'dummy',
			name: 'Dummy Money'
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_EX_is_not_object',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'dummy',
			name: 'Dummy Money',
			ex: 'test'
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_EX_is_empty',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'dummy',
			name: 'Dummy Money',
			ex: ''
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})


//ALIAS

	it('test_api_post_currency_returns_400_if_alias_is_missing',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			name: 'Dummy Money',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_alias_is_not_string',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 42,
			name: 'Dummy Money',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test_api_post_currency_returns_400_if_alias_is_empty',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "",
			name: 'Dummy Money',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

//Duplicate

	it('test_api_post_currency_returns_400_if_alias_is_duplicated',(done) => {

		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'usd',
			name: 'Dummy Money',
			ex: {
				'peso': 0.47,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err,res) => {
			assert.equal(res.status,400)
			done();
		})
	})

//No Duplicate
	it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err,res) => {
			assert.equal(res.status,200)
			done();
		})
	})

})
